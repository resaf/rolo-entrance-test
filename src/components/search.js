import React, { useState, useEffect } from 'react';
import Plus from '../assets/plus.png'

const Search = () => {
  return (
    <div className="inline-container">
      <div className="input-container">
        <input type="text" name="Search" className="input" />
      </div>
      <div className="search-icon-container">
        <img src={Plus} alt="plus" className="plus-icon"/>
      </div>
    </div>
  );
}

export default Search;
