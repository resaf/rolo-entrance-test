import React, { useState, useEffect } from 'react';
import ReceivedChat from './receivedChat'
import SentChat from './sentChat'


const ChatContainer = () => {
    return (
        <div className="chat-container">
            <ReceivedChat/>
            <SentChat/>
        </div>
    );
}

export default ChatContainer;
