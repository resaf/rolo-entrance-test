import React, { useState, useEffect } from 'react';
import Plus from '../assets/plus.png'

const ReceivedChat = () => {
    return (
        <div className="received-chat-container">
            <div className="received-bubble-chat">
                <div>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, </div>
            </div>
        </div>
    );
}

export default ReceivedChat;
