import React, { useState, useEffect } from 'react';

import Chat from '../assets/chat.png'
import Account from '../assets/accounts.png'
import Shutter from '../assets/shutter.png'
import Circle from '../assets/circle.png'

const LeftIconContainer = () => {
  return (
    <div className="column-container">
        <div className="icon-container-active">
            <img src={Chat} alt="chat" className="left-icons"/>
        </div>
        <div className="icon-container">
            <img src={Account} alt="chat" className="left-icons"/>
        </div>
        <div className="icon-container">
            <img src={Shutter} alt="chat" className="left-icons"/>
        </div>
        <div className="left-icon-absolute-container">
            <img src={Circle} alt="chat" className="left-icons-circle"/>
        </div>
    </div>
  );
}

export default LeftIconContainer;
