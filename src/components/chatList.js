import React, { useState, useEffect } from 'react';

import Man from '../assets/man-dummy-1.jpg'

const ChatList = () => {
    return (
        <div className="inline-container">
            <div className="chat-list-container">
                <div className="chat-profile-container">
                    <div className="large-profile-container">
                        <img src={Man} alt="chat" className="small-user-pic" />
                    </div>
                </div>
                <div className="chat-desc-container">
                    <div className="inline-container">
                        <div className="label-left-white">
                            <div>Hoang Do</div>
                        </div>
                        <div className="label-right-white">
                            <div>1h ago</div>
                        </div>
                    </div>
                    <div className="msg-preview-container">
                        <div className="msg-preview">Ps: Also works together with max-height, max-width, min-width and min-height css properties. It's espacially handy with using lenght units like 100% or 100vh/100vw to fill the container or the whole browser window.</div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default ChatList;
