import React, { useState, useEffect } from 'react';

import Man from '../assets/man-dummy-1.jpg'
import DownArrow from '../assets/expand-arrow.png'


const TopUserProfile = () => {
    return (
        <div className="inline-container">
            <div className="inline-right-content">
                <div className="user-profile-picture">
                    <div className="large-profile-container">
                        <img src={Man} alt="user-profile" className="small-user-pic" />
                    </div>
                </div>
                <div className="user-profile-detail">
                    <div className="column-content">
                        <div className="user-name">Eduardo Collin</div>
                        <div className="user-description">Business Partner</div>
                    </div>
                </div>
            </div>
            <div className="inline-left-content">
                <div className="small-round-button">
                    <img src={DownArrow} alt="user-profile" className="mini-icon" />
                </div>
            </div>
        </div>
    );
}

export default TopUserProfile;
