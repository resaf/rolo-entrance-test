import React, { useState, useEffect } from 'react';

import Microphone from '../assets/microphone.png'
import Cancel from '../assets/cancel.png'

const TextInput = () => {
    return (
        <div className="inline-container">
            <div className="input-container">
                <input type="text" name="Search" className="input" />
            </div>
            <div className="medium-icon-container">
                <div className="medium-round-white-button">
                    <img src={Cancel} alt="cancel" className="small-icon" />
                </div>
                <div className="medium-round-button">
                    <img src={Microphone} alt="microphone" className="medium-icon" />
                </div>
            </div>
        </div>
    );
}

export default TextInput;
