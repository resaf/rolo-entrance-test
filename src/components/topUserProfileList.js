import React, { useState, useEffect } from 'react';

import NakedPlus from '../assets/plus-naked.png'
import Man from '../assets/man-dummy-1.jpg'

const TopUserProfileList = () => {
    return (
        <div className="inline-container">
            <div className="mini-container">
                <div className="large-icon-container">
                    <img src={NakedPlus} alt="chat" className="large-plus-icon" />
                </div>
                <div className="small-text">add</div>
            </div>

            <div className="mini-container">
                <div className="large-profile-container">
                    <img src={Man} alt="chat" className="small-user-pic" />
                </div>
                <div className="small-text">Name</div>
            </div>
        </div>
    );
}

export default TopUserProfileList;
