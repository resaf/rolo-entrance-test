import React from 'react';
import logo from './logo.svg';
import './main-style.css';

import Search from './components/search'
import LeftIcons from './components/leftIconList'
import TopUserList from './components/topUserProfileList'
import ChatList from './components/chatList'
import TopUserProfile from './components/topUserProfile'
import ChatContainer from './components/chatContainer'
import TextInput from './components/textInput'

function App() {
  return (
    <div className="main-container">
      <div className="left-container">
        <div className="search-container">
          <Search />
        </div>
        <div className="left-content-container">
          <div className="left-icons-list-container">
            <LeftIcons />
          </div>
          <div className="left-message-list-container">
            <div className="profile-list-container">
              <TopUserList />
            </div>
            <div className="starred-msg-container">
              <div className="inline-container-fixed">
                <div className="label-left">
                  <div>Starred</div>
                </div>
                <div className="label-right">
                  <div>2 unread message</div>
                </div>
              </div>
              <ChatList />
            </div>
            <div className="regular-msg-container">
              <div className="inline-container-fixed">
                <div className="label-left">
                  <div>Messages</div>
                </div>
                <div className="label-right">
                  <div>2 unread message</div>
                </div>
              </div>
              <ChatList />
            </div>
          </div>
        </div>
      </div>
      <div className="right-container">
        <div className="user-profile-container">
          <TopUserProfile />
        </div>
        <div className="user-chat-container">
          <ChatContainer />
        </div>
        <div className="write-chat-container">
        <TextInput/>
        </div>
      </div>
    </div>
  );
}

export default App;
